# -*- coding: utf-8 -*-
"""
Created on Sun Jan  7 09:34:32 2018

@author: Jonathan Fry
"""

import json
#import sys

class Adf_ds():
       
    def __init__(self, name, type, linkedServiceName, tableName, external=True,
                 frequency='Day', interval=1):

        self.fname = name
        self.ds_dict = {'name':name,
                   'properties':{'type':type, 'external':external,
                                 'linkedServiceName':linkedServiceName, 
                                 'typeProperties':{'tableName':tableName}}, 
                                 'availability':{'frequency':frequency, 
                                                 'interval':interval}}

    def create_file(self):
        """Write out JSON for DS"""
        
        try:
            f = open(self.fname +'.json', mode='w')
            json.dump(self.ds_dict,f,indent=4)
        finally:
            f.close()

    def json_string(self):
        """return strong of JSON for DS"""
        return json.dumps(self.ds_dict)
     
    
#Adf_ds(name='test name', type='Oracle', linkedServiceName='ODS', tableName='test_tab' ).create_file()
#
#test = Adf_ds(name='test name', type='Oracle', linkedServiceName='ODS', tableName='test_tab' ).json_string()
#print(test)
#
##assert test =='{"name": "test name", "properties": {"type": "Oracle", "external": true, "linkedServiceName": "ODS", "typeProperties": {"tableName": "test_tab"}}, "availability": {"frequency": "Day", "interval": 1}}'
#
#test = Adf_ds(name='test name2', type='Oracle', linkedServiceName='ODS', tableName='test_tab2' )
#test.create_file()

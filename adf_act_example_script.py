# -*- coding: utf-8 -*-
"""
Created on Sat Jan 13 19:22:59 2018

@author: Jonathan Fry
"""
#Example of using the adf_oraact class

import pandas as pd
import adf_oraact as adf
    
df = pd.read_csv('D:/Geekdom/AnacondaProjects/activities.csv', header=0)
# =============================================================================
#df.head()
#df.info()
# =============================================================================

activities = adf.Adf_oraact()

for i, row in df.iterrows():
    
    activities.add_activity(name=row['name']
    , input_name=row['input_name']
    , output_name=row['output_name']
    , source_query=row['source_query'])

activities.create_file('test')
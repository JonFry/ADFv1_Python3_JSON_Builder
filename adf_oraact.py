# -*- coding: utf-8 -*-
"""
Created on Sun Jan  7 09:34:32 2018

@author: Jonathan Fry
"""

import json
#import sys

class Adf_oraact():
       
    def __init__(self):
        self.act_list = []
#        self._fname = name
#        _act_desc = self.__generate_desc(description, act_type, input_name, output_name)
        
#        self._ds_dict = {'name':name, 'description':_act_desc,'type':act_type,
#                   'inputs':[{'name':input_name}], 'outputs':[{'name':output_name}],        
#                                 'typeProperties':{'source':{'type':source_type, 'oracleReaderQuery':source_query}, 'sink':{'type':sink_type}}, 
#                                 'policy':{'concurrency':1, 'executionPriorityOrder':'OldestFirst', 'retry':5}}

    def __generate_desc(self, description, act_type, input_name, output_name):
        if description=='':
             desc = act_type + ' from ' + input_name +' to ' + output_name
        else:   
            desc = description
        return desc
            
    def add_activity(self, name, input_name, output_name, source_query,
                 source_type='OracleSource', sink_type='AzureDataLakeStoreSink', 
                 description='', act_type='Copy', frequency='Day', interval=1):
        
        _act_desc = self.__generate_desc(description, act_type, input_name, output_name)
        
        self.act_list.append({'name':name, 'description':_act_desc,'type':act_type,
                   'inputs':[{'name':input_name}], 'outputs':[{'name':output_name}],        
                                 'typeProperties':{'source':{'type':source_type, 'oracleReaderQuery':source_query}, 'sink':{'type':sink_type}}, 
                                 'policy':{'concurrency':1, 'executionPriorityOrder':'OldestFirst', 'retry':5}})
    
    def create_file(self, fname):
        """Write out JSON for activites"""
        try:
            f = open(fname +'_activity.json', mode='w')
            json.dump(self.act_list,f,indent=4)
        finally:
            f.close()

    def json_string(self):
        """return string of JSON for DS"""
        return json.dumps(self.act_list,indent=4)
     
#
#test=Adf_oraact(name='testORA', input_name='TestInp', output_name='TestOut', source_query='SELECT * FROM DUAL;').json_string()
#print(test)
#
#Adf_oraact(name='testORA', input_name='TestInp', output_name='TestOut', source_query='SELECT * FROM DUAL;').create_file()
#
#test = Adf_ds(name='test name', type='Oracle', linkedServiceName='ODS', tableName='test_tab' ).json_string()
#print(test)
#
#assert test =='{"name": "test name", "properties": {"type": "Oracle", "external": true, "linkedServiceName": "ODS", "typeProperties": {"tableName": "test_tab"}}, "availability": {"frequency": "Day", "interval": 1}}'
#
#
#test = Adf_ds(name='test name2', type='Oracle', linkedServiceName='ODS', tableName='test_tab2' )
#print(test.create_file())

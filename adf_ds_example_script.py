# -*- coding: utf-8 -*-
"""
Created on Sat Jan 13 19:22:59 2018

@author: Jonathan Fry
"""
#Example of using the adf_ds class

import pandas as pd
import adf_ds as adf
    
df = pd.read_csv('D:/Geekdom/AnacondaProjects/datasets.csv', header=0)
# =============================================================================
# #df.head()
# #df.info()
# 
# #i=1
# #print(df.iloc[i]['name'])
# =============================================================================

for i, row in df.iterrows():
    
    adf.Adf_ds(name=row['name']
    , type=row['type']
    , linkedServiceName=row['linkedServiceName']
    , tableName=row['tableName']
    , external=bool(row['external'])).create_file()


